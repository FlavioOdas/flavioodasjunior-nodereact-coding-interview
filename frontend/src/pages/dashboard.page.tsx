import React, { FC, useState, useEffect, useContext } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { SearchBar } from "../components/search-bar";
import { CircularProgress } from "@mui/material";

import { BackendClient } from "../clients/backend.client";
import { SearchBarProvider, searchContext } from "../contexts/searchContext";

const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [filteredUsers, setFilteredUsers] = useState<IUserProps[]>([]);
  const [loading, setLoading] = useState(true);
  const context = useContext(searchContext);

  useEffect(() => {
    const fetchData = async () => {
      const result = await backendClient.getAllUsers();
      setUsers(result.data);

      setLoading(false);
    };

    fetchData();
  }, []);

  useEffect(() => {
    const usersFromSearch = users.filter((value) => {
      return;
    });

    setFilteredUsers();
  }, [searchContext]);

  return (
    <SearchBarProvider>
      <div style={{ paddingTop: "30px" }}>
        <SearchBar />

        <div style={{ display: "flex", justifyContent: "space-between" }}>
          {loading ? (
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                height: "100vh",
              }}
            >
              <CircularProgress size="60px" />
            </div>
          ) : (
            <div>
              {users.length
                ? users.map((user) => {
                    return <UserCard key={user.id} {...user} />;
                  })
                : null}
            </div>
          )}
        </div>
      </div>
    </SearchBarProvider>
  );
};
