export interface ISearch {
  currentFilter?: string;
  search: string;
}

export interface ISearchContextProvider {
  setSearchValue: React.Dispatch<React.SetStateAction<ISearch>>;
}
