import React, { FC, useContext } from "react";
import { IUserProps } from "../../dtos/user.dto";
import { TextField } from "@material-ui/core";
import { searchContext } from "../../contexts/searchContext";

export const SearchBar: React.FC = () => {
  const context = useContext(searchContext);

  return (
    <TextField
      id="outlined-basic"
      label="Outlined"
      variant="outlined"
      onChange={(event) =>
        context.setSearchValue({ search: event.target.value })
      }
    />
  );
};
