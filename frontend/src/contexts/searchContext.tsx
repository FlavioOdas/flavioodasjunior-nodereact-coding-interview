import React, { FC, createContext, useState, useContext } from "react";
import { ISearch, ISearchContextProvider } from "../dtos/search.dto";

export const searchContext = createContext<ISearch & ISearchContextProvider>({
  currentFilter: "title",
  search: "",
  setSearchValue: () => {},
});

export const SearchBarProvider: React.FC = ({ children }) => {
  const [searchValue, setSearchValue] = useState<ISearch>({
    currentFilter: "title",
    search: "",
  });

  return (
    <searchContext.Provider
      children={children}
      value={{ ...searchValue, setSearchValue }}
    />
  );
};
